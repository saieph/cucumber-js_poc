var protractor = require('protractor')
, webdriver = require('selenium-webdriver')
    , SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;

var World = (function() {

  var pathToSeleniumJar = "C:/Users/rbedino/WebstormProjects/selenium-server-standalone-2.46.0.jar";
  var wdHub = "http://127.0.0.1:4444";
  var server = new SeleniumServer(pathToSeleniumJar, {
      port: 4444
  });
  server.start();

    var timeout = 15000;

  var firefoxOptions = webdriver.Capabilities.firefox();
  var chromeOptions = webdriver.Capabilities.chrome();
    //options['caps_'].options={
    //    args: ['--url-base=/wd/hub']
    //};

  var driver = new webdriver.Builder()
      .usingServer(server.address())
      .withCapabilities(firefoxOptions)
      .build();

  function World(callback) {

    driver.manage().timeouts().setScriptTimeout(timeout);

      var winHandleBefore;

      driver.getWindowHandle().then(function(result){
          winHandleBefore = result;
      });

    this.browser = protractor.wrapDriver(driver);
    this.protractor = protractor;
    this.by = protractor.By;
    this.By = webdriver.By;

    callback();

    this.quit = function(callback){
      driver.quit().then(function(){
        callback();  
      });
    }
  }

  return World;
});

module.exports.world = World;

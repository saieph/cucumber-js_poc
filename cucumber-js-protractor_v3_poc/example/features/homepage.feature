Feature: POC V2 Paywall loging and navigation plus validation

  Scenario Outline: POC
    Given I am on the homepage
    And I enter the "Merchant Account" as '<merchantAccount>'
    And I enter the "Caller Name" as '<callerName>'
    And I enter the "Secret Key" as '<secretKey>'
    And I click the "Login" button
    And I select the "Merchant Dropdown" as '<account>'
    And I click the "Example Paywall" button
    And I select the "Country" as '<country>'
    And I enter the "Price" as '<price>'
    And I enter the "Currency" as '<currency>'
    And I enter the "ID" as '<id>'
    And I click the "Submit" button
    And I click the '<pymtMethod>' payment method
    And I enter the "first card digits" as '<card_digit_1>'
    And I enter the "second card digits" as '<card_digit_2>'
    And I enter the "third card digits" as '<card_digit_3>'
    And I enter the "fourth card digits" as '<card_digit_4>'
    And I click the "checkbox" for '<checkbox>'
    And I click the "pay" button
    Then I have completed validating '<thisScenario>'

    Examples:
    |merchantAccount|callerName|secretKey     |account |country|price|currency|id    |pymtMethod |card_digit_1|card_digit_2|card_digit_3|card_digit_4|checkbox         |thisScenario|
    |TWOFISH        |$rbedino  |Fedaykin666!!!|CrowFall|AUSTRIA|1    |EUR     |random|paysafecard|0000        |0000        |0070        |2860        |ps_agree_to_terms|admin login |
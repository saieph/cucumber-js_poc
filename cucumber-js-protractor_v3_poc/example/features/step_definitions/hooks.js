var pc = require('../../conf.js');

var hooks = function() {
  this.World = pc.world();

  this.After(function(callback) {
    this.quit(callback);
  });
};

module.exports = hooks;
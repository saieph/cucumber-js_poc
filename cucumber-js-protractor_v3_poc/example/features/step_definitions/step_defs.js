var step_defs = function() {

    var random5 = Math.random().toString(36).substring(2, 7);


    this.Given(/^I am on the homepage$/, function(callback) {
        this.browser.get('https://payplusdev.tfelements.com/');
        setTimeout(callback, 12000);
    });

    this.When(/^I (?:select|enter|choose|click) the "([^"]*)" (?:as|for) '(.*)'$/, function (itemName, itemText, callback) {
        switch(itemName){
            case "Merchant Account":
                itemName = "login.merchantAccount";
                var merchantAccount = this.browser.findElement(this.by.model(itemName));
                merchantAccount.sendKeys(itemText);
                setTimeout(callback, 500);
                break;
            case "Caller Name":
                itemName = "login.callerName";
                var callerName = this.browser.findElement(this.by.model(itemName));
                callerName.sendKeys(itemText);
                setTimeout(callback, 500);
                break;
            case "Secret Key":
                itemName = "login.secretKey";
                var secretKey = this.browser.findElement(this.by.model(itemName));
                secretKey.sendKeys(itemText);
                setTimeout(callback, 500);
                break;
            case "Merchant Dropdown":
                this.browser.findElement(this.by.cssContainingText('option', itemText)).click();
                setTimeout(callback, 2000);
                break;
            case "Country":
                this.browser.findElement(this.by.cssContainingText('option', itemText)).click();
                setTimeout(callback, 500);
                break;
            case "Price":
                itemName = 'paywallConfig.price';
                var priceField = this.browser.findElement(this.by.model(itemName));
                priceField.click();
                priceField.clear();
                priceField.sendKeys(itemText);
                setTimeout(callback, 300);
                break;
            case "Currency":
                this.browser.findElement(this.by.cssContainingText('option', itemText)).click();
                setTimeout(callback, 300);
                break;
            case "ID":
                itemName = 'paywallConfig.externalKey';
                this.browser.findElement(this.by.model(itemName)).sendKeys(random5);
                setTimeout(callback, 500);
                break;
            case "first card digits":
                this.browser.switchTo().frame('paywallFrame');
                this.browser.ignoreSynchronization = true;
                itemName = 'pinForm:rn01';
                this.browser.findElement(this.By.id(itemName)).sendKeys(itemText);
                this.browser.switchTo().defaultContent();
                setTimeout(callback, 500);
                break;
            case "second card digits":
                this.browser.switchTo().frame('paywallFrame');
                this.browser.ignoreSynchronization = true;
                itemName = 'pinForm:rn02';
                this.browser.findElement(this.By.id(itemName)).sendKeys(itemText);
                this.browser.switchTo().defaultContent();
                setTimeout(callback, 500);
                break;
            case "third card digits":
                this.browser.switchTo().frame('paywallFrame');
                this.browser.ignoreSynchronization = true;
                itemName = 'pinForm:rn03';
                this.browser.findElement(this.By.id(itemName)).sendKeys(itemText);
                this.browser.switchTo().defaultContent();
                setTimeout(callback, 500);
                break;
            case "fourth card digits":
                this.browser.switchTo().frame('paywallFrame');
                this.browser.ignoreSynchronization = true;
                itemName = 'pinForm:rn04';
                this.browser.findElement(this.By.id(itemName)).sendKeys(itemText);
                this.browser.switchTo().defaultContent();
                setTimeout(callback, 500);
                break;
            case "checkbox":
                this.browser.switchTo().frame('paywallFrame');
                this.browser.ignoreSynchronization = true;
                switch(itemText){
                    case"ps_agree_to_terms":
                        var checkboxCheck = this.browser.findElement(this.By.id('pinForm:agb-overlay'));
                        checkboxCheck.click();
                        break;
                }
                this.browser.switchTo().defaultContent();
                setTimeout(callback, 500);
                break;
            default:
                break;
        }
    });

    this.When(/^I click the "([^"]*)" (?:button|link)$/, function (itemName, callback) {
        switch(itemName){
            case "Login":
                var loginButton = this.browser.findElement(this.by.buttonText(itemName));
                loginButton.click();
                setTimeout(callback, 2000);
                break;
            case "Submit":
                itemName = '/html/body/div[1]/div/div[2]/div/div[4]/div/div[1]/accordion/div/div[1]/div[2]/div/div/div/div/ng-form/div[5]/input';
                var submitButton = this.browser.findElement(this.By.xpath(itemName));
                submitButton.click();
                setTimeout(callback, 12000);
                break;
            case "Payment Methods":
                itemName = "payment methods";
                this.browser.findElement(this.by.cssContainingText('h4', itemName)).click();
                setTimeout(callback, 2000);
                break;
            case "Example Paywall":
                this.browser.findElement(this.by.cssContainingText('h4', itemName)).click();
                setTimeout(callback, 4000);
                break;
            case "pay":
                this.browser.switchTo().frame('paywallFrame');
                this.browser.ignoreSynchronization = true;
                this.browser.findElement(this.By.id('pinForm:pay')).click();
                setTimeout(callback, 6000);
                break;
            default:
                break;
        }
    });

    this.When(/^I click the '(.*)' payment method$/, function (pymtMethod, callback) {
        this.browser.switchTo().frame('paywallFrame');
        this.browser.ignoreSynchronization = true;
        switch(pymtMethod){
            case "paysafecard":
                pymtMethod = "PAYSAFECARD";
                break;
            default:
                break;
        }
        var methodButton = this.browser.findElement(this.By.className(pymtMethod));
        methodButton.click();
        this.browser.switchTo().defaultContent();
        setTimeout(callback, 10000);
    });

    this.Then(/^I have completed validating '(.*)'$/, function (validTest, callback) {
        switch(validTest){
            case "admin login":
                console.log("admin login complete");
                setTimeout(callback, 1000);
                break;
            default:
                break;
        }
    });

};

module.exports = step_defs;
